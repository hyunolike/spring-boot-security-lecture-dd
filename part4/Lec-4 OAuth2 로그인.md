# OAuth2

스프링은 OAuth2 에 대해 많은 기본 기능들을 제공합니다. 

## CommonOAuth2Provider
> 정보를 미리 제공한데!! 오오오오오 신기하다
아래 4개의 Provider 에 대해 기본 정보들을 제공합니다.

- GOOGLE : https://console.cloud.google.com/
- GITHUB : https://github.com/settings/applications/new
- FACEBOOK : https://developers.facebook.com/
- OKTA

## 추가 가능한 OAuth2Provider ...

- naver : https://developers.naver.com/
- kakao : https://developers.kakao.com/

## OAuth2User

- facebook, naver, kakao
- OAuth2User : UserDetails 를 대체합니다.
- OAuth2UserService : UserDetailsService 를 대체합니다. 기본 구현체는 DefaultOAuth2UserService 입니다.

<img src="../images/fig-36-oauth2-service.png" width="500" style="max-width:500px;width:100%;" />

## OidcUser 

- google ✅
- OidcUser
- OidcUserService : OAuth2UserService 를 확장한 서비스

## (추가) 옥탑방 개발자 유튜브 참고한거
> 그니까 아래코드는 기존 사이트 유저에 sns유저 매핑하는 거야 ✅
- [깃허브 소스코드 자료](https://github.com/jongwon/spring-security-junit5-test)
- [유투브](https://www.youtube.com/watch?v=87208XkWG-s&list=PLcaKom3xthg63Qq5qCG7EG7XY3nN05ypd&index=13)
- ![image.png](./image.png)
